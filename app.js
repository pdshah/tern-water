var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost', 
    user: 'root',
    password: '1234',
    database: 'tern' 
});

// create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({extended: false}));
// parse various different custom JSON types as JSON
app.use(bodyParser.json());

connection.connect(function(err){
if(err) {
    console.log("Error connecting databse");
}
else {
    console.log("Database connection established");
}
});

app.get('/', function(req, res){
	res.send('Hello World');
});

// GET request view devices available to user 
app.get('/devices', function(req, res){
	var data = {
		"error":1, 
		"Devices":""
	};
    
	connection.query('SELECT * FROM devices AS d JOIN location AS l ON d.location_id=l.location_id', function(err, rows, fields){
   		if(err) throw err;
		if(rows.length != 0) {
			data["error"] = 0;
			data["Devices"] = rows;
			res.json(data);
		}
	});
});

//POST request to add new devices
app.post('/devices', function(req, res) {
    var name = req.body.name;
    var serial = req.body.serial;
    var zipcode = req.body.zipcode;
    var location_id;
    var data = {
       "error":1,
       "Devices":""
   };
    
   var location_promise = new Promise(function(resolve, reject){
        // Finds location from location table to properly input to device table
        // NOT YET IMPLEMENTED: When user enters unknown zip code
       connection.query('SELECT * FROM location WHERE zipcode=?', [zipcode], function(err, row, field){
           if(err) throw err;
           if (row != 0){
               location_id = row[0].location_id;
                // !! ensures boolean statment
               if(!!location_id && !!name && !!serial){
                   resolve(location_id);
               }
               else{
                   reject(Error("No location found"));
               }
           }
       });
   });
    
   location_promise.then(function(result){
       connection.query('INSERT INTO devices(serialnumber, device_name, user_id, location_id) VALUES(?,?,?,?)', [serial, name, 1, result], function(err, row, field) {
           if(err) throw err;
           data["error"] = 0;
           data["Devices"] = "Succesful Add";
           res.json(data);
       });
   }, function(err){
       console.log(err);
   });
});

//app.update('/devices', function(req, res){
    //var name = req.body.name;
    //var zipcode = req.body.zip;
    //var location_id;
    //var data = {
    //    "error":1,
    //    "Devices":""
    //};
    
    //var location_promise = new Promise(function(resolve, reject){
        //Finds location from location table to properly input to device table
        //NOT YET IMPLEMENTED: When user enters unknown zip code
        //connection.query('SELECT * FROM location WHERE zipcode=?', [zipcode], function(err, row, field){
            //if(err) throw err;
            //if (row != 0){
                //location_id = row[0].location_id;
                //!! ensures boolean statment
                //if(!!location_id && !!name && !!serial){
                   // resolve(location_id);
                //}
                //else{
                 //   reject(Error("No location found"));
                //}
           // }
       // });
    //});
    
    //location_promise.then(function(result){
        //connection.query('UPDATE devices SET device_name=?, location_id=?) VALUES(?,?)', [name, result], function(err, row, field) {
            //if(err) throw err;
            //data["error"] = 0;
            //data["Devices"] = "Succesful Add";
 //           res.json(data);
 //       });
 //   }, function(err){
 //       console.log(err);
 //   });
//});
    
    

app.listen(3000, function() {
	console.log('Listening on 3000');
});
