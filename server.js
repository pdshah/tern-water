var dweetClient = require("node-dweetio");
var express = require('express');
var mysql = require('mysql');
var moment = require('moment');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
server.listen(80);

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'mydb'
});

connection.connect(function(err){
    if(err){
        console.log("Error connecting databse");
    }
    else {
        console.log("Database connection established");
        listen('ternSmartFaucet1');
        listen('ternSmartFaucet2');
    }
});

app.use(express.static('public'))

function listen(deviceName) {
    
    var dweetio = new dweetClient();
    
    //Listening for data
    dweetio.listen_for(deviceName, function(dweet) {
        // Parse through data
        var device = dweet.thing;
        var timestamp = dweet.created;
        var action = dweet.content.action;
        var temp = dweet.content.temp;
        var consumption = dweet.content.consumption;
        var batteryLevel = dweet.content.battery_level;
        var deviceId = dweet.content.device_id;
        
        return new Promise(function(resolve, reject) {
               // Insert into SQL log database
           var query = connection.query('INSERT INTO ternsmartfaucet1log(time, action, battery_level, temp, consumption, device_id) VALUES(?, ?, ?, ?, ?,?)',[timestamp, action, batteryLevel, temp, consumption, deviceId], function(err, row, fields){
                if(err) reject(err);
                resolve();
            });
        }).then(function() {
            return new Promise(function(resolve, reject){
                // get total consumption
                var statement = connection.query('SELECT sum(consumption) AS totalConsumption FROM ternSmartFaucet1log WHERE device_id=?',[deviceId],  function(err, rows, fields){
                    if(err) reject(err);
                    if (rows != 0){
                        var totalConsumption = rows[0].totalConsumption; 
                        resolve(totalConsumption); 
                    }
                    else{
                        reject(new Error('ERR!'));
                    }
                });
            });
        }).then(function(result){
            var health = 100-((2.5)*(result));
            return new Promise(function(resolve, reject) {
                 var query = connection.query('UPDATE ternSmartFaucet1status SET time=?, battery=?, action=?, health=?, consumption_total=?, temp=? WHERE device_id=?', [timestamp, batteryLevel, action, health, result, temp, deviceId], function(err, rows, fields){
                    if(err) reject(err);
                    io.emit('newData', deviceId);
                    resolve();
                });
            });
        }).catch(function(error) {
            io.emit('error', error);
        });
    });
}

io.on('connection', function(socket) {
    socket.on('query', function(data) {
        return new Promise(function(resolve, reject) {
            if(data == 1001) {
                io.emit('name', 'ternSmartFaucet1')
            }
            else if(data == 1002) {
                io.emit('name', 'ternSmartFaucet2')
            }
            var query = connection.query('SELECT * from mydb.ternsmartfaucet1status WHERE device_id=?', [data], function(err, rows, fields){
                if(err) reject(err);
                io.emit('battery', rows[0].battery);
                io.emit('action', rows[0].action);
                io.emit('health', rows[0].health);
                io.emit('consumption_total', rows[0].consumption_total.toFixed(2));
                io.emit('temp', rows[0].temp)
                resolve();
            });
        }).then(function() {
        var weeklyConsumptionDays = [];
        for(i = 0; i <7; i++) {
                var day;
                if(i==0) {
                    day = moment().format('YYYY-MM-DD').substring(1);
                }
                else {
                    day = moment().subtract(i, 'days').format('YYYY-MM-DD').substring(1);
                }
                weeklyConsumptionDays.push(new Promise(function(resolve, reject) {
                    connection.query("SELECT sum(consumption) as day FROM ternSmartFaucet1log WHERE time LIKE '%" + day +"%' AND device_id=?",[data], function(err, rows, fields){
                        if (err) {
                            reject(err);
                        }
                        
                        if (rows[0].day == null) {
                            resolve(0);
                        }
                        else {
                            resolve(rows[0].day.toFixed(2));
                        }
                    });
                }));
            }
            return Promise.all(weeklyConsumptionDays);
    }).then(function(weeklyConsumption) {
        io.emit('weeklyConsumption', weeklyConsumption);
    }).catch(function(error) {
        io.emit('error', error);
    });
        
    });
 console.log('socket connected');
});
