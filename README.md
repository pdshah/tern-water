README

### Development of Wifi-Enabled Monitoring and Purification Smart-Device for Residential Water (In Progress) ###

To provide proof of concept:
Server.js file reads data from Arduino (with three temperature sensors) connected via serial port to display real-time data on a webpage, index.html

Frameworks/Libraries Used:
Bootstrap
socket.io
express

![Capture2.PNG](https://bitbucket.org/repo/q4qdpL/images/3611387497-Capture2.PNG)

Mobile Application in Progress:
Contents located in Tern App Folder.
Built via Ionic Framework

API in Progress:
app.js connects to local mySQL database to perform GET an POST requests to display user's devices and add new devices
