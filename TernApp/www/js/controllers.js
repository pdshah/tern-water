angular.module('app.controllers', [])
  
.controller('loginCtrl', function($scope, $state) {
	var authProvider = 'basic'
	var authSetting = {'remember': true}
	var credentials = {
		'email': $scope.email,
		'password': $scope.password
	}
	var authSuccess = function(user) {
		console.log(Ionic.User.current());
		$state.go("devices");
	}
	var authFailure = function(){
		for (var err in errors){
			console.log(err);
		}
	}
	var login = function(){
		Ionic.Auth.login(authProvider, authSetting, credentials).then(authSuccess, authFailure);
	}
})
   
.controller('signupCtrl', function($scope) {
	$scope.user = {}
	var signupSuccess = function(user) {
		console.log(user);
		//$state.go("devices");
	}
	var signupFailure = function(user){
	for (var err in errors){
		console.log(err);
	}
	$scope.signup = function(user){
		var name = $scope.user.name;
		var email = $scope.user.email;
		var password = $scope.user.password;
		user = {'email': email, 'password': password};
		user.custom = {'name': name};
		console.log(user);
		Ionic.Auth.signup(user).then(signupSuccess, signupFailure);

	}
		
		
		// var user = {
		// 'email': $scope.email,
		// 'password': $scope.password
		// }
		// user.custom = {
		// 'name': $scope.name
		// }
		// Ionic.Auth.signup(user).then(signupSuccess, signupFailure);
	}

})
   
.controller('myTernCtrl', function($scope) {

})
   
.controller('ourTernCtrl', function($scope) {

})
   
.controller('welcomeToTern!Ctrl', function($scope) {

})
   
.controller('addProductCtrl', function($scope, $state, $ionicPopup,$http) {
	$scope.devices = devices;
    $scope.newDevice = {};
	$scope.addProduct = function(newDevice) {
        var name = $scope.newDevice.name;
        var serial = $scope.newDevice.serial;
        var zip = $scope.newDevice.zip;

        if (!(/^\d+$/.test(zip) && zip.length == 5)) {
            var error = 'zip code';
            showAlert(error);
        }
        else if(name == null){
            var error = 'name';
            showAlert(error);
        }
        else {
		  newDevice = {device_name: name, serial: serial, zipcode: zip};
		  console.log(newDevice);
	   	  $scope.devices.push(newDevice);
          $state.go("devices")
          $scope.newDevice = {};
        }
	}
    
    var showAlert = function(error) {
        var alert = $ionicPopup.alert({
            title: "Error",
            template: 'There is a problem with your ' + error + '. Please try again.'
        });
        alert.then(function(res){
            console.log('Alert successful');
        });
    }

})
      
.controller('devicesCtrl', function($scope, $http, $ionicPopup, $ionicListDelegate) {
	$http.get("http://localhost:3000/devices").success(function(res){
		$scope.devices = res.Devices;
	});
    $scope.devices = devices;
	$scope.edit = function(device) {
	   $scope.data = {name: device.device_name, zip: device.zipcode};

	   // An elaborate, custom popup
	   var myPopup = $ionicPopup.show({
	     template: ['Device Name:<input ng-model="data.name"> Zip Code: <input ng-model="data.zip">'],
	     title: 'Edit Your Device',
	     scope: $scope,
	     buttons: [
	       { text: 'Cancel',
	       	 onTap: $ionicListDelegate.closeOptionButtons() },
	       {
	         text: '<b>Save</b>',
	         type: 'button-balanced',
	         onTap: function(e) {
	           $ionicListDelegate.closeOptionButtons();
	           if (!($scope.data.name && $scope.data.zip)) {
	             //don't allow the user to close unless he enters wifi password
	             e.preventDefault();
	           } else {
	             return $scope.data;
                 console.log($scope.data);
	           }
	           $ionicListDelegate.closeOptionButtons();
	         }
	       },
	     ]
	   });
	   myPopup.then(function(res) {
	     console.log('Tapped!', res);
	     device.device_name = res.name;
	     device.zipcode = res.zip;
	   });

  };


})
   
.controller('everyoneCtrl', function($scope) {

})
 
 var devices = [
		{device_name: "Smart Faucet 1", serial: "111", zipcode: "19104"},
		{device_name: "Smart Faucet 2", serial: "222", zipcode: "19103"},
		{device_name: "Smart Faucet 3", serial: "333", zipcode: "19102"}
	];