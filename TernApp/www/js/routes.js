angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl'
    })
        
    .state('signup', {
      url: '/signup',
      templateUrl: 'templates/signup.html',
      controller: 'signupCtrl'
    })
         
    .state('tabsController.myTern', {
      url: '/personal',
      views: {
        'tab1': {
          templateUrl: 'templates/myTern.html',
          controller: 'myTernCtrl'
        }
      }
    })
        
    .state('tabsController.ourTern', {
      url: '/Community',
      views: {
        'tab2': {
          templateUrl: 'templates/ourTern.html',
          controller: 'ourTernCtrl'
        }
      }
    })
          
    .state('welcomeToTern!', {
      url: '/welcome',
      templateUrl: 'templates/welcomeToTern!.html',
      controller: 'welcomeToTern!Ctrl'
    })
        
    .state('addAProduct', {
      url: '/addproduct',
      templateUrl: 'templates/addAProduct.html',
      controller: 'addProductCtrl'
    })
  
    .state('tabsController', {
      url: '/Tabs',
      abstract:true,
      templateUrl: 'templates/tabsController.html'
    })
        
    .state('devices', {
      url: '/devices',
      templateUrl: 'templates/devices.html',
      controller: 'devicesCtrl'
    })
       
    .state('tabsController.everyone', {
      url: '/World',
      views: {
        'tab3': {
          templateUrl: 'templates/everyone.html',
          controller: 'everyoneCtrl'
        }
      }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});