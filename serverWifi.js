var dweetClient = require("node-dweetio");
var express = require('express');
var mysql = require('mysql');
var moment = require('moment');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
server.listen(80);

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'mydb'
});

connection.connect(function(err){
    if(err){
        console.log("Error connecting databse");
    }
    else {
        console.log("Database connection established");
        listen('ternSmartFaucet1');
        // listen('ternSmartSink2');
    }
});

// app.get('/', function (req, res) {
//     res.sendFile(__dirname + '/index.html');
// });

app.use(express.static('public'));

function listen(deviceName) {
    //var databaseTable = deviceName + 'log';
    
    var dweetio = new dweetClient();
    
    
    //Listening for data
    dweetio.listen_for(deviceName, function(dweet) {
        // Parse through data
        var device = dweet.thing;
        var timestamp = dweet.created;
        var action = dweet.content.action;
        var temp = dweet.content.temp;
        var consumption = dweet.content.consumption;
        var batteryLevel = dweet.content.battery_level;
        //var deviceId = dweet.content.device_id;
        
    //     function emit(key, data) {
    //     io.emit(deviceName, {
    //         key: key,
    //         data: data
    //     });
    // }
        return new Promise(function(resolve, reject) {
               // Insert into SQL log database
           var query = connection.query('INSERT INTO ternsmartfaucet1log(time, action, battery_level, temp, consumption) VALUES(?, ?, ?, ?, ?)',[timestamp, action, batteryLevel, temp, consumption], function(err, row, fields){
                if(err) reject(err);
                console.log('Data Logged');
                resolve();
            });
        }).then(function() {
            return new Promise(function(resolve, reject){
                // get total consumption
                connection.query('SELECT sum(consumption) AS totalConsumption FROM ternSmartFaucet1log',  function(err, rows, fields){
                    if(err) reject(err);
                    console.log(err);
                    if (rows != 0){
                        var totalConsumption = rows[0].totalConsumption; 
                        //console.log(totalConsumption);
                        resolve(totalConsumption); 
                    }
                    else{
                        reject(new Error('ERR!'));
                    }
                });
            });
        }).then(function(result){
            var health = 100-((2.5)*(result));
            //console.log(health);
            return new Promise(function(resolve, reject) {
                 connection.query('INSERT into ternSmartFaucet1status(time, battery, action, health, consumption_total) VALUES(?, ?, ?, ?, ?)', [timestamp, batteryLevel, action, health, result], function(err, rows, fields){
                    if(err) reject(err);
                    resolve();
                });
            });
        }).then(function() {
            return new Promise(function(resolve, reject) {
                connection.query('SELECT * from mydb.ternsmartfaucet1status ORDER BY idternSmartFaucet1Status DESC LIMIT 1', function(err, rows){
                    if(err) reject(err);
                    io.emit('battery', rows[0].battery);
                    io.emit('action', rows[0].action);
                    io.emit('health', rows[0].health);
                    io.emit('consumption_total', rows[0].consumption_total);
                    resolve();
                });
            });
        }).then(function() {
            var weeklyConsumptionDays = [];
            
            for(i = 0; i <7; i++) {
                    var day;
                    if(i==0) {
                        day = moment().format('YYYY-MM-DD').substring(1);
                        console.log(day);
                    }
                    else {
                        day = moment().subtract(i, 'days').format('YYYY-MM-DD').substring(1);
                    }
                    
                    weeklyConsumptionDays.push(new Promise(function(resolve, reject) {
                        connection.query("SELECT sum(consumption) as day FROM ternSmartFaucet1log WHERE time LIKE '%" + day +"%'", function(err, rows, fields){
                            if (err) {
                                reject(err);
                            }
                            
                            if (rows[0].day == null) {
                                resolve(0);
                            }
                            else {
                                //console.log(rows[0].day0);
                                console.log(rows[0].day);
                                
                                resolve(rows[0].day);
                                //socket.emit('day0', rows[0].day0);
                            }
                        });
                    }));
            }
            return Promise.all(weeklyConsumptionDays);
        }).then(function(weeklyConsumption) {
            //console.log(weeklyConsumption);
            io.emit('weeklyConsumption', weeklyConsumption);
        }).catch(function(error) {
            io.emit('error', error);
        });
    });
}

io.on('connection', function(socket) {
    console.log('socket connected');
});

// app.use('/',function(request,response){
//    response.sendFile(__dirname + '/index.html');
// });
