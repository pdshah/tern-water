var app = require('http').createServer(handler)
var io = require('socket.io')(app);
connect = require('connect')
var fs = require('fs');

app.listen(80);

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

io.on('connection', function (socket) {
  console.log('connected')
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data, callback) {
  if (err) {
    callback({error:'someErrorCode', msg:'Some message'});
    return;
  }

  callback({ok:true});
    
  });
});